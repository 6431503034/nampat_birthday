// NextPage.js
import React from 'react';
import defaultImage from './image/sis.jpg'; // รูปภาพที่คุณต้องการ
import './NextPage.css'; // ไฟล์ CSS เพื่อใช้สไตล์

function NextPage({ onClose }) { // แก้ชื่อ prop เป็น onClose
    const handleClose = () => {
        onClose();
    }

    return (
        <div className="nextPageContainer">
            <h1>เบิร์ดเดย์จ้า<span role="img" aria-label="party popper">🎉</span><span role="img" aria-label="party popper">🎉</span><span role="img" aria-label="party popper">🎉</span></h1>

            {/* เพิ่มรูปภาพและกล่องข้อความ */}
            <img src={defaultImage} alt="Profile" className="nextPageImage" />
            <div className="nextPageText">
                <p>สวย ๆ รวย ๆ การงานราบรื่น</p>
                <p>สุขภาพร่างกายแข็งแรง</p>
                <p>อยู่เลี้ยงข้าวชั้นไปนาน ๆ 555555</p>
                <p>Wishing you all the best on your birthday!</p>
            </div>

            {/* เปลี่ยนปุ่ม Back เป็นปุ่ม Close */}
            <button className="close-button" onClick={handleClose}>Close</button>
        </div>
    );
}

export default NextPage;
