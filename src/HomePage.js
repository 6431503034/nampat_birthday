import React, { useState } from 'react';
import defaultImage from './image/minioncake.jpg'; // แก้เป็น URL ของรูปภาพที่คุณต้องการ
import './HomePage.css'; // นำเข้าไฟล์ CSS เพื่อใช้งานสไตล์

function HomePage({ onNext }) {
    const [birthday, setBirthday] = useState('');

    const handleNext = () => {
        if (birthday.trim() !== '') {
            onNext();
        } else {
            alert('Please enter your birthday.');
        }
    }

    return (
        <div className="homePageContainer">
            <h1>Nampat Birthday App</h1>
            <img src={defaultImage} alt="Default" />
            <div className="formContainer">
            <p className="input-label">Enter your birthday:</p>
            <input type="date" className="input-date" value={birthday} onChange={(e) => setBirthday(e.target.value)} />
            <button className="next-button" onClick={handleNext}>Next</button>

            </div>
        </div>
    );
}

export default HomePage;
