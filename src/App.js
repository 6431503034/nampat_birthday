import React, { useState } from 'react';
import HomePage from './HomePage';
import NextPage from './NextPage';
import './App.css';

function App() {
  const [currentPage, setCurrentPage] = useState('home');

  const handleNextPage = () => {
    setCurrentPage('next');
  }

  const handleBackPage = () => {
    setCurrentPage('home');
  }

  return (
    <div className="App">
      {currentPage === 'home' && <HomePage onNext={handleNextPage} />}
      {currentPage === 'next' && (
        <div>
          <NextPage onBack={handleBackPage} />
        </div>
      )}
    </div>
  );
}

export default App;
